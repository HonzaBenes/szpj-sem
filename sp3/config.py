from os import path


class Config:
    root = 'sp3'

    models_dir = path.join(root, 'models')
    documents_dir = path.join(root, 'data', 'documents')
    output_dir = path.join(root, 'out')

    reference_file = path.join(root, 'data', 'cacm_devel.rel')
    query_file = path.join(root, 'data', 'query_devel.xml')
    vectorizer = path.join(models_dir, 'vectorizer.pickle')
    documents = path.join(models_dir, 'documents.npz')
    output_file = path.join(output_dir, 'results.txt')
