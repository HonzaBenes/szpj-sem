from os import listdir, path
from sklearn.feature_extraction.text import TfidfVectorizer

from sp3.config import Config
from sp3.utils import save_sparse_csr, save_pickle, create_dir


def build_corpus(documents_dir):
    corpus = []

    for document_name in sorted(listdir(documents_dir)):
        with open(path.join(documents_dir, document_name), 'r', encoding='utf-8') as handler:
            corpus.append(handler.read())

    return corpus


if __name__ == '__main__':
    config = Config()

    create_dir(config.models_dir)

    # Vectorizer which omits the numbers
    vectorizer = TfidfVectorizer(ngram_range=(1, 1), sublinear_tf=True, norm='l2', analyzer='word')

    corpus = build_corpus(config.documents_dir)

    print('Building the data matrix using the TfidfVectorizer')
    documents = vectorizer.fit_transform(corpus)

    print('Saving the vectorizer to file')
    save_pickle(config.vectorizer, vectorizer)

    print("Saving the vectorized documents")
    save_sparse_csr(config.documents, documents)
