import numpy as np
from sklearn.metrics.pairwise import cosine_similarity

from sp3.config import Config
from sp3.utils import create_dir, load_sparse_csr, load_pickle


def process_queries(file_path):
    corpus = {}
    current_query = ""
    current_i = -1

    with open(file_path, 'r', encoding='utf-8') as handler:
        for line in handler:
            line = line.strip()
            if line == '</DOC>':
                assert current_i != -1, f"AssertionError: Index not assigned: {current_i}"
                corpus[current_i] = current_query
                current_query = ""
                current_i = -1
            elif line.startswith("<DOCNO>"):
                current_i = int(line.split()[1])
            elif not line.startswith('<DOC') and line:
                current_query += " " + line

    return corpus


if __name__ == '__main__':
    config = Config()

    N = 100

    vectorizer = load_pickle(config.vectorizer)
    documents = load_sparse_csr(config.documents)

    create_dir(config.output_dir)

    queries = process_queries(config.query_file)

    with open(config.output_file, 'w') as f:
        for query_i, raw_query in queries.items():
            query = vectorizer.transform([raw_query])
            sim = cosine_similarity(query, documents).flatten()

            # - operator for descending order, selecting only 100 most similar
            ind = np.argsort(-sim)[0:N]
            for i in range(N):
                document_i = ind[i]
                f.write(f'{query_i}\tCACM-{document_i + 1:04}\t{sim[document_i]}\n')

        print(f'Results saved to {config.output_file}')
