import nltk
from nltk.collocations import *

from sp2.main import save_latex_table

if __name__ == '__main__':
    with open("sp2/data/cno_15.txt", 'r') as f:
        data = f.read()

    # 0) Filter the data
    text_list = [t for t in nltk.word_tokenize(data) if t.isalpha()]
    text = nltk.Text(text_list)

    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_words(text)
    finder.apply_freq_filter(5)
    scored = finder.score_ngrams(bigram_measures.pmi)

    save_latex_table(scored[:20], 'sp2/referat/tables/pmi_5_reference.tex', headers=("Bigram", r"$log_{2}(Ppst)$"))
