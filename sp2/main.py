import operator
import nltk
import tabulate
from math import log


def get_counts(items):
    counts = {}
    for item in items:
        if item not in counts:
            counts[item] = 0
        counts[item] += 1
    return counts


def compute_probs(counts, N):
    probs = {}
    for item, count in counts.items():
        probs[item] = count / N
    return probs


def bigram_5_generator(text_list, unigram_counts):
    for bigram in nltk.bigrams(text_list):
        if unigram_counts[bigram[0]] >= 5 and unigram_counts[bigram[1]] >= 5:
            yield bigram


def compute_PMI(bigram_generator, unigram_probs):
    PMI = {}
    for bigram in bigram_generator:
        PMI[bigram] = log(P_ML[bigram] / (unigram_probs[bigram[0]] * unigram_probs[bigram[1]]), 2)
    return PMI


def save_latex_table(data, file_location, sci_notation=False, headers=("Bigram", "Ppst")):
    fmt = ".2E" if sci_notation else ".2f"
    table = tabulate.tabulate(data, tablefmt="latex_raw", floatfmt=fmt, numalign="center", headers=headers)
    with open(file_location, 'w') as ft:
        ft.write(table)


if __name__ == "__main__":
    # with open(args.input_file, 'r') as f:
    with open("sp2/data/cno_15.txt", 'r') as f:
        data = f.read()

    # 0) Filter the data
    text_list = [t for t in nltk.word_tokenize(data) if t.isalpha()]

    # 1) P_{ML)(w_i, w_j) = C(w_i, w_j)/N
    bigram_counts = get_counts(nltk.bigrams(text_list))

    N = len(text_list)
    P_ML = compute_probs(bigram_counts, N)

    # 2) PMI(w_i, w_j) = log_{2} P(w_{i}, w_{j})/P{w_{i}, w_{j})

    unigram_counts = get_counts(text_list)

    unigram_probs = compute_probs(unigram_counts, N)

    PMI = compute_PMI(nltk.bigrams(text_list), unigram_probs)

    # 3) PMI of bigrams where each bigram constituent occurs at least 5 times in the corpus
    PMI_5 = compute_PMI(bigram_5_generator(text_list, unigram_counts), unigram_probs)

    # 4) Sort, print and save tables

    P_ML_sorted = sorted(P_ML.items(), key=operator.itemgetter(1), reverse=True)
    PMI_sorted = sorted(PMI.items(), key=operator.itemgetter(1), reverse=True)
    PMI_5_sorted = sorted(PMI_5.items(), key=operator.itemgetter(1), reverse=True)

    print(f"P_ML:\n{P_ML_sorted[:20]}")
    print(f"PMI:\n{PMI_sorted[:20]}")
    print(f"PMI_5:\n{PMI_5_sorted[:20]}")

    save_latex_table(P_ML_sorted[:20], 'sp2/referat/tables/p_ml.tex', sci_notation=True)
    save_latex_table(PMI_sorted[:20], 'sp2/referat/tables/pmi.tex', headers=("Bigram", r"$log_{2}(Ppst)$"))
    save_latex_table(PMI_5_sorted[:20], 'sp2/referat/tables/pmi_5.tex', headers=("Bigram", r"$log_{2}(Ppst)$"))
